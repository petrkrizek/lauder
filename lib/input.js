const readline = require('readline')

const readLine = () =>
  new Promise((resolve, reject) => {
    const rl = readline.createInterface({
      input: process.stdin,
      // output: process.stdout,
    })

    rl.question('', function (answer) {
      resolve(answer)
      rl.close()
    })
  })

exports.readLine = readLine
