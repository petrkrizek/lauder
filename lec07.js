// tohle je komentar
/*
tohle taky
na vic radku
*/



function multiplyByTwo(x) {
  console.log('vstup do fce je', x)
  let y = 2 * x
  console.log('vystup z fce je', y)
  // return muze vracet promennou
  return y
}

let result = multiplyByTwo(-200)
let result2 = multiplyByTwo(100)
console.log('result', result)
console.log('result2', result2)

let bumbum = 'outside'
let bumbum2 = 'outside'
console.log('bumbum po definici', bumbum, bumbum2)


// scope promennych je definovan chlupatymi zavorkami
if (123 === 123) {
  let bumbum = 'inside if'
  bumbum2 = 'inside if'
  console.log('bumbum v ifu', bumbum, bumbum2)
}

console.log('bumbum po if venku', bumbum, bumbum2)

function greeting(name) {
  console.log('ahoj, jak se mas,', name, '?')
  let bumbum = 'inside greeting fn'
  bumbum2 = 'inside greeting fn'
  console.log('bumbum uvnitr fce', bumbum, bumbum2)
  // fce nema return, tzn. nic nevraci
}

let pozdrav = greeting('pepo')
console.log('pozdrav je prazdny, protoze fce nema return', pozdrav )

function multiplyAndAddOneHundred(numA, numB, numC) {
  // return muze vracet primo vypocet
  return (numA * numB * numC) + 100
}

let res = multiplyAndAddOneHundred(5, 10, 15)

console.log('vysledek nasobeni je:', res)

// fce pro vypocet obvodu kruhu pro dane r - polomer
function obvodKruhu(r) {
  console.log('vstupni polomer je ', r)
  return 2 * r * Math.PI
}


let obvodPrvnihoKruhu = obvodKruhu(5)
console.log('obvodPrvnihoKruhu s polomerem 5 je', obvodPrvnihoKruhu)

// jako argument fce muze byt i promenna ne jen hodnota
let obvodDruhehoKruhu = obvodKruhu(obvodPrvnihoKruhu)
console.log('obvodDruhehoKruhu je', obvodDruhehoKruhu)