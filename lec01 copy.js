const { readLine } = require('./lib/input')

async function start() {
  while (true) {
    goStraight()

    if (isPathOnTheLeft()) {
      turnLeft()
    }
  }
}

start()

function isPathOnTheLeft() {
  return true
}


function turnLeft() {
  console.log('zahni doleva',)
}

function goStraight() {
  console.log('jdi rovne',)
}