function prevodStupnuCelsiaNaFahrenheita(c) {
  let f = (c * 9) / 5 + 32
  console.log(c, 'stupnu celsia je', f, 'stupnu fahrenheita')
  return f
}

function fahrenheitNaCelsia(f) {
  let c = ((f - 32) * 5) / 9
  console.log(f, 'stupnu fahrenheita je', c, 'stupnu fahrenheita')
  return c
}

// fce prevodStupnu, ...2, ...3 jsou si ekvivalentni a delaji to same
export function prevodStupnu(stupne, naCelsius) {
  if (naCelsius) {
    return fahrenheitNaCelsia(stupne)
  } else {
    return prevodStupnuCelsiaNaFahrenheita(stupne)
  }
}

export function prevodStupnu2(stupne, naCelsius) {
  let res
  if (naCelsius) {
    res = fahrenheitNaCelsia(stupne)
  } else {
    res = prevodStupnuCelsiaNaFahrenheita(stupne)
  }

  return res
}

function prevodStupnu3(stupne, naCelsius) {
  return naCelsius
    ? fahrenheitNaCelsia(stupne)
    : prevodStupnuCelsiaNaFahrenheita(stupne)
}

let res = prevodStupnu(212, false)

console.log('vysledek je ', res)

export default function myFunc(params) {
  console.log('asdf')
}
