const { readLine } = require('../lib/input')

async function start() {
  console.log("what's your name?")
  const read = await readLine()
  console.log('Hello', read)
  const read2 = await readLine()
  console.log('Hello', read2)
}

start()
