// import library for reading user input
var readLine = require('./input').readLine

async function start() {
  // sem budeme psat nas kod

  console.log('zadejte jmeno')
  
  // pockame na uzivatele, co napise. Po zmacknuti enteru, se to, co napsal uzivatel, ulozi do promenne name
  var name = await readLine() 

  console.log('Vitame te, ', name)
  console.log('Zadej vek')
  var age = await readLine()

  if (age >= 18) {
    console.log('To jste tady spravne!')
  } else {
    console.log('ne ', name, 'tvuj vek', age, 'neni dostatecny. Koncim hru :(')
    return // skonci program
  }

  console.log(
    'Mame tady sirokou nabidku zbrani do tveho portfolia. Muzes si vybrat z mece, luku, sekyry. Co si vyberes? [mec, sekyra, luk]'
  )

  var weapon = await readLine()

  var damage = 1
  var swingPerTurn = 5
  if (weapon === 'mec') {
    console.log(weapon, 'je perfektni volba')
    damage = 10
    swingPerTurn = 4
  } else if (weapon === 'luk') {
    console.log(weapon, 'je perfektni volba')
    damage = 8
    swingPerTurn = 5
  } else if (weapon === 'sekyra') {
    damage = 13
    swingPerTurn = 3
    console.log(weapon, 'je perfektni volba')
  } else {
    console.log('hmm', weapon, 'neni zbran, smula, pouzivas ruce!')
  }

  console.log(
    'tvuj damage je',
    damage,
    'a utocis',
    swingPerTurn,
    'x za kolo. Celkem udelas',
    damage * swingPerTurn,
    'damage za kolo.'
  )


  console.log('kam pujdes? [les, dedina]')

  var whereTo
  var jeNerozhodnuty = true
  
  // while je cyklus, ktery nam bezi furt dokola, pokud plati to v zavorce
  // zde tedy, pokud je promenna jeNerozhodnuty true, tak se cyklus opakuje
  while(jeNerozhodnuty) {
    // 
    whereTo = await readLine()

    if (whereTo === 'les') {
      console.log('les je dobra volba', )
      // pokud uzivatel odpovedel tak, jak chceme, nastavime, ze se 'rozhodnul', tedy jeNerozhodnuty je nepravda
      jeNerozhodnuty = false 
    } else if (whereTo === 'dedina' ) {
      console.log('kdo se boji nesmi do lesa')
      jeNerozhodnuty = false
    } else {
      // uzivatel neodpovedel tak, jak chceme, nemenime tedy hodnotu promenne 'jeNerozhodnuty' a ta zustava true
      console.log('tam nemuzes jit. zkus to znova!')
    }

    // konec jednoho behu cyklu, ted program skoci zpet na radek 65 (kde zacina cyklus) a kontroluje, zda stale plati podminka v zavorce
    // pokud neplati, cyklus se ukoncuje a pokracujeme kodem za zavorkou. 
    // Pokud stale plati, cyklus bezi od zacatku.
  }

  // tady pokracujeme po skonceni cyklu
  console.log('vydal ses na misto zvane ', whereTo, ' a ....')
  
  // domaci ukol:
  // pokracujte ve hre, zkuste nacist 2 uzivatelske vstupy (odpovedi na nejakou vasi otazku) 
  // a neco s nimi udelat... 

  // tymova prace je dovolena, ale neni nutna. 
  // na dalsi hodine muzete prezentovat za body
}

start()
