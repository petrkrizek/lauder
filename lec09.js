function rollDice(range) { //vytvori funkci rollDice s argumentem range, ktery predstavuje rozsah, ktery chceme, aby melo nahodne cislo vracene funkci
    let rawRandom = Math.random() * range //vytvori promenou rawRandon, ktera je nasobkem Math.Random, ktere vraci hodnotu mezi 0 a 1 a argumentem funkce range
    let random = Math.ceil(rawRandom) //vytvori promenou random, jejiz hodnota je hodnota promene rawrandom zaokrouhlena na cele cislo smerem nahoru
    return random 
    // funkce vrati hodnotu promene random
}

// let rollDice = (range) => {
//     let rawRandom = Math.random() * range
//     let random = Math.ceil(rawRandom)
//     return random
// } 
// //funkce rollDice, ale zapsana jako arrow function

// let rollDice = (range) => Math.ceil(Math.random() * range)


//funkce rollDice zapsana jako arrow function na jednom radku

let num = 0
while(num < 10) {
  console.log('ahoj', )
  num = num + 1
}

while(true) {
  let res = rollDice(6)
  console.log(res)
  if (res === 6) {
    break
  }
}

//spousti funkci rollDice a vypisuje, co nam vratila, dokud funkce nevrati 6
// process.exit() //ukonci program
console.log(rollDice(10)) //vypise to, co nam vrati funkce rollDice s argumentem 10


let map = [
  [0, 0, 1, 0],
  [2, 2, 5, 2],
  [0, 0, 0, 0],
  [1, 2, 1, 2],
] //vytvori dvourozmerny seznam se jmenem map
console.log(map[1][2]) //vypise treti polozku se seznamu, ktery je druhou polozkou seznamu map


let players = ['Karel', 'Jarda', 'Artom', 'Adam'] //vytvori seznam se jmenem Players a s polozkami Karel, Jarda, Artom, Adam

console.log(players) //vypise cely seznam
players[4] = 'Vojta' //prida do seznamu polozku vojta na pate misto
console.log(players) //vypise seznam

players[1] = 'Pepek' //prida na druhe misto seznamu polozku Pepek
console.log(players.length) //vypise seznam
players[players.length] = 'nakonci' // pridani na konec

console.log(players) //vzpise seznam
players.splice(1, 1) //vymaze jednu polozku na ctvrte pozici

console.log(players) //vypise seznam
players.push('Natalie') //prida na konec seznamu polozku Natalie
console.log(players) //vypise seznam
players.unshift('Jarmila') //prida na zacatek seznamu polozku Jarmila, indexy nasledujicich polozek se zmeni o +1
console.log(players) //vypise seznam

players.splice(2, 3, 'Karolina') //prida na treti misto polozku Karolina

console.log(players) // vypise seznam


console.log('------') //vypise: ------

let posledni = players.pop()
console.log(players) // vypise seznam
console.log('posledni byl:', posledni) //odebere posledni polozku seznamu a vypise ji

process.exit()
console.log(players.shift()) //odebere prvni polozku seznamu a vypise ji


players.forEach((player) => {
  console.log('hrac:', player)
})
//pro kazdou polozku seznamu vypise hrac: a danou polozku

console.log('Players length: ', players.length) //vypise Players lenght: a pocet polozek v seznamu

let i = 0 //vytvori novou promenou i a priradi ji hodnotu 0
while (i < players.length) {
  console.log(players[i])
  i += 1
}
// dokud je i mensi nez delka seznamu, vypisuje polozky seznamu, v jednom cyklu vypise polozku seznamu o indexu i a zvetsi hodnotu i o 1, vypise vsechny polozky seznamu, protoze zacina cislem 0, vzdy se zvysi o 1 a konci hodnotou o 1 mensi, nez je delka seznamu, coz je poslednu polozka seznamu, protoze se seznam cisluje od 0

while (players.length > 0) {
  console.log(players)
  players.splice(0, 1)
}
// dokud je delka seznamu players vetsi nez nula, vypise seznam a vymaze prvni polozku seznamu
