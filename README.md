# DevStack našeho semináře

## Postup prvního spuštění

- otevřu si konzoli - zkratka  ```Ctrl + Shift + P``` a napsat ```Terminal: Focus next terminal``` 
- do terminalu napisu ```npm i```
- az to dobehne, napisu ```npm run develop```
- (prikazy vyse piseme bez ```)
- v konzoli bychom meli videt otazku 'whats your name?'


## Co dále?
- zjistit k čemu jsou další soubory v této složce (google)
- celá magie se děje v souboru lec01.js, můžete ho prozkoumat a zkoušet co se stane, pokud něco změníte.



