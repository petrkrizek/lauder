// napad: poznamenavat si casy vstupu a zjistovat jak casto tam chodi (kolik je cas jednoho nakupu)
// zjistit min max, prumer, median, pocet lidi v prodejne, pocet zaznamu,...
const readline = require('readline')

const readLine = () =>
  new Promise((resolve, reject) => {
    const rl = readline.createInterface({
      input: process.stdin,
      // output: process.stdout,
    })

    rl.question('', function (answer) {
      resolve(answer)
      rl.close()
    })
  })

async function start() {
  console.log(
    'Zadej kladne cislo pro procet lidi, co vstoupilo do prodejny. Zaporne pro ty co odesli. Zadej c pro ziskani soucasneho mnozstvi lidi v prodejne. Zadej s pro ziskani statistik. Pro konec napis end.'
  )

  let allValues = []

  let value
  while (value !== 'end') {
    value = await readLine()

    if (!value) {
      console.log('zadna hodnota')
      continue
    }

    if (value === 'end') {
      console.log('konec')
      break
    }

    if (value === 'c') {
      console.log('aktualne v prodejne', getSum(allValues))
      continue
    }

    if (value === 's') {
      console.log('Statistiky: TODO: bude implementovano')
      continue
    }

    // TODO: validace vstupu, validace pozitivniho poctu

    allValues.push(parseInt(value))

    console.log('aktualne v prodejne', getSum(allValues))
  }
}

start()

function getSum(arr) {
  return arr.reduce((sum, curr) => {
    return sum + curr
  }, 0)
}
